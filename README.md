# .eslintrc file

A definition of rules for my projects

## Usage

### ESLint 3.0^

Paste this line to your ``package.json`` file, into ``devDependencies``:

```javascript
"mdziekon-eslintrc": "git@gitlab.com:mdziekon/eslintrc.git#3.0.0"
```

Add ``extends`` property in the top level object of ``.eslintrc`` file:

```javascript
{
    "extends": [
        "./node_modules/mdziekon-eslintrc/.eslintrc"
    ]
}
```
