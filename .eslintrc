{
    "parser": "babel-eslint",
    "parserOptions": {
        "ecmaVersion": 6,
        "sourceType": "module",
        "ecmaFeatures": {
            "impliedStrict": true,
            "experimentalObjectRestSpread": true
        }
    },
    "env": {
        "node": true,
        "es6": true
    },
    "plugins": [],

    "rules": {
        // Possible Errors
        "no-cond-assign": [
            "error",
            "except-parens"
        ],
        "no-console": [
            "off",
            {
                // "allow": []
            }
        ],
        "no-constant-condition": [
            "error",
            {
                "checkLoops": true
            }
        ],
        "no-control-regex": [
            "error"
        ],
        "no-debugger": [
            "error"
        ],
        "no-dupe-args": [
            "error"
        ],
        "no-dupe-keys": [
            "error"
        ],
        "no-duplicate-case": [
            "error"
        ],
        "no-empty": [
            "error",
            {
                "allowEmptyCatch": false
            }
        ],
        "no-empty-character-class": [
            "error"
        ],
        "no-ex-assign": [
            "error"
        ],
        "no-extra-boolean-cast": [
            "error"
        ],
        "no-extra-parens": [
            "off"
        ],
        "no-extra-semi": [
            "error"
        ],
        "no-func-assign": [
            "error"
        ],
        "no-inner-declarations": [
            "error",
            "both"
        ],
        "no-invalid-regexp": [
            "error",
            {
                "allowConstructorFlags": []
            }
        ],
        "no-irregular-whitespace": [
            "error",
            {
                "skipStrings": false,
                "skipComments": false,
                "skipRegExps": false,
                "skipTemplates": false
            }
        ],
        "no-negated-in-lhs": [
            "error"
        ],
        "no-obj-calls": [
            "error"
        ],
        "no-prototype-builtins": [
            "off"
        ],
        "no-regex-spaces": [
            "error"
        ],
        "no-sparse-arrays": [
            "error"
        ],
        "no-unexpected-multiline": [
            "error"
        ],
        "no-unreachable": [
            "error"
        ],
        "no-unsafe-finally": [
            "error"
        ],
        "use-isnan": [
            "error"
        ],
        "valid-jsdoc": [
            "off"
        ],
        "valid-typeof": [
            "error"
        ],

        // Best Practices
        "accessor-pairs": [
            "off"
        ],
        "array-callback-return": [
            "error"
        ],
        "block-scoped-var": [
            "error"
        ],
        "complexity": [
            "off"
        ],
        "consistent-return": [
            "off",
            {
                "treatUndefinedAsUnspecified": false
            }
        ],
        "curly": [
            "error",
            "all"
        ],
        "default-case": [
            "off"
        ],
        "dot-location": [
            "error",
            "property"
        ],
        "dot-notation": [
            "error",
            {
                "allowKeywords": true,
                "allowPattern": "^[a-z]+((_|\\-).*?[a-z]+)+$"
            }
        ],
        "eqeqeq": [
            "error",
            "always"
        ],
        "guard-for-in": [
            "error"
        ],
        "no-alert": [
            "error"
        ],
        "no-caller": [
            "error"
        ],
        "no-case-declarations": [
            "error"
        ],
        "no-div-regex": [
            "error"
        ],
        "no-else-return": [
            "error"
        ],
        "no-empty-function": [
            "error",
            {
                "allow": []
            }
        ],
        "no-empty-pattern": [
            "error"
        ],
        "no-eq-null": [
            "error"
        ],
        "no-eval": [
            "error",
            {
                "allowIndirect": false
            }
        ],
        "no-extend-native": [
            "error",
            {
                "exceptions": []
            }
        ],
        "no-extra-bind": [
            "error"
        ],
        "no-extra-label": [
            "error"
        ],
        "no-fallthrough": [
            "error"
        ],
        "no-floating-decimal": [
            "error"
        ],
        "no-implicit-coercion": [
            "error",
            {
                "boolean": true,
                "number": true,
                "string": true,
                "allow": []
            }
        ],
        "no-implicit-globals": [
            "error"
        ],
        "no-implied-eval": [
            "error"
        ],
        "no-invalid-this": [
            "error"
        ],
        "no-iterator": [
            "error"
        ],
        "no-labels": [
            "error",
            {
                "allowLoop": false,
                "allowSwitch": false
            }
        ],
        "no-lone-blocks": [
            "error"
        ],
        "no-loop-func": [
            "error"
        ],
        "no-magic-numbers": [
            "error",
            {
                "ignore": [ 1 ],
                "ignoreArrayIndexes": true,
                "enforceConst": true,
                "detectObjects": false
            }
        ],
        "no-multi-spaces": [
            "error",
            {
                "exceptions": {}
            }
        ],
        "no-multi-str": [
            "error"
        ],
        "no-native-reassign": [
            "error",
            {
                "exceptions": []
            }
        ],
        "no-new": [
            "error"
        ],
        "no-new-func": [
            "error"
        ],
        "no-new-wrappers": [
            "error"
        ],
        "no-octal": [
            "error"
        ],
        "no-octal-escape": [
            "error"
        ],
        "no-param-reassign": [
            "error",
            {
                "props": true
            }
        ],
        "no-proto": [
            "error"
        ],
        "no-redeclare": [
            "error",
            {
                "builtinGlobals": true
            }
        ],
        "no-return-assign": [
            "error",
            "always"
        ],
        "no-script-url": [
            "error"
        ],
        "no-self-assign": [
            "error"
        ],
        "no-self-compare": [
            "error"
        ],
        "no-sequences": [
            "error"
        ],
        "no-throw-literal": [
            "error"
        ],
        "no-unmodified-loop-condition": [
            "error"
        ],
        "no-unused-expressions": [
            "error",
            {
                "allowShortCircuit": false,
                "allowTernary": false
            }
        ],
        "no-unused-labels": [
            "error"
        ],
        "no-useless-call": [
            "error"
        ],
        "no-useless-concat": [
            "error"
        ],
        "no-useless-escape": [
            "error"
        ],
        "no-void": [
            "error"
        ],
        "no-warning-comments": [
            "warn",
            {
                "terms": [ "FIXME", "TODO" ],
                "location": "start"
            }
        ],
        "no-with": [
            "error"
        ],
        "radix": [
            "error",
            "always"
        ],
        "vars-on-top": [
            "error"
        ],
        "wrap-iife": [
            "error",
            "inside"
        ],
        "yoda": [
            "error",
            "never"
        ],

        // Strict Mode
        "strict": [
            "error",
            "never"
        ],

        // Variables
        "init-declarations": [
            "off"
        ],
        "no-catch-shadow": [
            "error"
        ],
        "no-delete-var": [
            "error"
        ],
        "no-label-var": [
            "error"
        ],
        "no-restricted-globals": [
            "error",
            "event", "fdescribe"
        ],
        "no-shadow": [
            "error",
            {
                "builtinGlobals": false,
                "hoist": "all",
                "allow": []
            }
        ],
        "no-shadow-restricted-names": [
            "error"
        ],
        "no-undef": [
            "error",
            {
                "typeof": true
            }
        ],
        "no-undef-init": [
            "error"
        ],
        "no-undefined": [
            "off"
        ],
        "no-unused-vars": [
            "error",
            {
                "vars": "local",
                "args": "none",
                "caughtErrors": "none"
            }
        ],
        "no-use-before-define": [
            "error",
            {
                "functions": true,
                "classes": true
            }
        ],

        // Node.js and CommonJS
        "callback-return": [
            "error"
        ],
        "global-require": [
            "error"
        ],
        "handle-callback-err": [
            "error",
            "^.*(e|E)rr"
        ],
        "no-mixed-requires": [
            "error",
            {
                "grouping": true,
                "allowCall": true
            }
        ],
        "no-new-require": [
            "error"
        ],
        "no-path-concat": [
            "error"
        ],
        "no-process-env": [
            "error"
        ],
        "no-process-exit": [
            "error"
        ],
        "no-restricted-modules": [
            "error",
            "os"
        ],
        "no-sync": [
            "error"
        ],

        // Stylistic Issues
        "array-bracket-spacing": [
            "error",
            "always"
        ],
        "block-spacing": [
            "error",
            "always"
        ],
        "brace-style": [
            "error",
            "1tbs"
        ],
        "camelcase": [
            "error",
            {
                "properties": "always"
            }
        ],
        "comma-dangle": [
            "error",
            "never"
        ],
        "comma-spacing": [
            "error",
            {
                "before": false,
                "after": true
            }
        ],
        "comma-style": [
            "error",
            "last"
        ],
        "computed-property-spacing": [
            "error",
            "never"
        ],
        "consistent-this": [
            "off"
        ],
        "eol-last": [
            "error",
            "unix"
        ],
        "func-names": [
            "error",
            "always"
        ],
        "func-style": [
            "off"
        ],
        "id-blacklist": [
            "error",
            "e", "cb"
        ],
        "id-length": [
            "error",
            {
                "min": 2,
                "max": 1000,
                "properties": "always",
                "exceptions": [ "x", "y", "z", "i", "j", "k", "n" ]
            }
        ],
        "id-match": [
            "off"
        ],
        "indent": [
            "error",
            4,
            {
                "VariableDeclarator": {
                    "let": 1,
                    "var": 1,
                    "const": 1
                },
                "SwitchCase": 0,
                "outerIIFEBody": 4,

            }
        ],
        "jsx-quotes": [
            "error",
            "prefer-double"
        ],
        "key-spacing": [
            "error",
            {
                "beforeColon": false,
                "afterColon": true,
                "mode": "strict"
            }
        ],
        "keyword-spacing": [
            "error",
            {
                "before": true,
                "after": true
            }
        ],
        "linebreak-style": [
            "error",
            "unix"
        ],
        "lines-around-comment": [
            "error",
            {
                "beforeBlockComment": true,
                "afterBlockComment": false,
                "beforeLineComment": false,
                "afterLineComment": false,
                "allowBlockStart": true,
                "allowBlockEnd": true,
                "allowObjectStart": true,
                "allowObjectEnd": true,
                "allowArrayStart": true,
                "allowArrayEnd": true
            }
        ],
        "max-depth": [
            "warn",
            4
        ],
        "max-len": [
            "error",
            {
                "code": 80,
                "tabWidth": 4,
                "comments": 80,
                "ignorePattern": undefined,
                "ignoreComments": false,
                "ignoreTrailingComments": false,
                "ignoreUrls": true
            }
        ],
        "max-lines": [
            "off",
            {
                "max": 300,
                "skipBlankLines": true,
                "skipComments": true
            }
        ],
        "max-nested-callbacks": [
            "error",
            3
        ],
        "max-params": [
            "error",
            5
        ],
        "max-statements": [
            "off",
            10,
            {
                "ignoreTopLevelFunctions": true
            }
        ],
        "max-statements-per-line": [
            "error",
            {
                "max": 1
            }
        ],
        "new-cap": [
            "error",
            {
                "newIsCap": true,
                "capIsNew": false,
                "newIsCapExceptions": [
                    "constructor"
                ],
                "capIsNewExceptions": [
                    // Defaults (like Primitives' names) still applied
                ],
                "properties": true
            }
        ],
        "new-parens": [
            "error"
        ],
        "newline-after-var": [
            "error",
            "always"
        ],
        "newline-before-return": [
            "error"
        ],
        "newline-per-chained-call": [
            "error",
            {
                "ignoreChainWithDepth": 1
            }
        ],
        "no-array-constructor": [
            "error"
        ],
        "no-bitwise": [
            "error",
            {
                "allow": [],
                "int32Hint": false
            }
        ],
        "no-continue": [
            "off"
        ],
        "no-inline-comments": [
            "error"
        ],
        "no-lonely-if": [
            "error"
        ],
        "no-mixed-operators": [
            "error",
            {
                // "groups": [
                //     ["+", "-", "*", "/", "%", "**"],
                //     ["&", "|", "^", "~", "<<", ">>", ">>>"],
                //     ["==", "!=", "===", "!==", ">", ">=", "<", "<="],
                //     ["&&", "||"],
                //     ["in", "instanceof"]
                // ],
                "allowSamePrecedence": true
            }
        ],
        "no-mixed-spaces-and-tabs": [
            "error"
        ],
        "no-multiple-empty-lines": [
            "error",
            {
                "max": 2,
                "maxEOF": 1,
                "maxBOF": 1
            }
        ],
        "no-negated-condition": [
            "off"
        ],
        "no-nested-ternary": [
            "error"
        ],
        "no-new-object": [
            "error"
        ],
        "no-plusplus": [
            "off",
            {
                "allowForLoopAfterthoughts": true
            }
        ],
        "no-restricted-syntax": [
            "error",
            "WithStatement"
        ],
        "no-spaced-func": [
            "error"
        ],
        "no-ternary": [
            "off"
        ],
        "no-trailing-spaces": [
            "error",
            {
                "skipBlankLines": false
            }
        ],
        "no-underscore-dangle": [
            "error",
            {
                "allow": [],
                "allowAfterThis": true
            }
        ],
        "no-unneeded-ternary": [
            "error"
        ],
        "no-whitespace-before-property": [
            "error"
        ],
        "object-curly-newline": [
            "off",
            {
                "multiline": true,
                "minProperties": 2
            }
        ],
        "object-curly-spacing": [
            "error",
            "always",
            {
                "objectsInObjects": true,
                "arraysInObjects": true
            }
        ],
        "object-property-newline": [
            "error",
            {
                "allowMultiplePropertiesPerLine": false
            }
        ],
        "one-var": [
            "error",
            "never"
        ],
        "one-var-declaration-per-line": [
            "error",
            "always"
        ],
        "operator-assignment": [
            "off"
        ],
        "operator-linebreak": [
            "error",
            "after"
        ],
        "padded-blocks": [
            "error",
            "never"
        ],
        "quote-props": [
            "error",
            "as-needed"
        ],
        "quotes": [
            "error",
            "single"
        ],
        "require-jsdoc": [
            "off"
        ],
        "semi": [
            "error",
            "always"
        ],
        "semi-spacing": [
            "error",
            {
                "before": false,
                "after": true
            }
        ],
        "sort-vars": [
            "off"
        ],
        "space-before-blocks": [
            "error",
            "always"
        ],
        "space-before-function-paren": [
            "error",
            "always"
        ],
        "space-in-parens": [
            "error",
            "never"
        ],
        "space-infix-ops": [
            "error",
            {
                "int32Hint": false
            }
        ],
        "space-unary-ops": [
            "error",
            {
                "words": true,
                "nonwords": false,
                "overrides": {}
            }
        ],
        "spaced-comment": [
            "error",
            "always",
            {
                "exceptions": [ "*" ],
                "markers": []
            }
        ],
        "unicode-bom": [
            "error",
            "never"
        ],
        "wrap-regex": [
            "error"
        ],

        // ECMAScript 6
        "arrow-body-style": [
            "error",
            "as-needed",
            {
                "requireReturnForObjectLiteral": true
            }
        ],
        "arrow-parens": [
            "error",
            "always"
        ],
        "arrow-spacing": [
            "error",
            {
                "before": true,
                "after": true
            }
        ],
        "constructor-super": [
            "error"
        ],
        "generator-star-spacing": [
            "error",
            {
                "before": false,
                "after": true
            }
        ],
        "no-class-assign": [
            "error"
        ],
        "no-confusing-arrow": [
            "error",
            {
                "allowParens": true
            }
        ],
        "no-const-assign": [
            "error"
        ],
        "no-dupe-class-members": [
            "error"
        ],
        "no-duplicate-imports": [
            "error",
            {
                "includeExports": false
            }
        ],
        "no-new-symbol": [
            "error"
        ],
        "no-restricted-imports": [
            "error",
            "os"
        ],
        "no-this-before-super": [
            "error"
        ],
        "no-useless-computed-key": [
            "error"
        ],
        "no-useless-constructor": [
            "error"
        ],
        "no-useless-rename": [
            "error",
            {
                "ignoreDestructuring": false,
                "ignoreImport": false,
                "ignoreExport": false
            }
        ],
        "no-var": [
            "error"
        ],
        "object-shorthand": [
            "error",
            "always",
            {
                "avoidQuotes": true,
                "ignoreConstructors": false
            }
        ],
        "prefer-arrow-callback": [
            "error",
            {
                "allowNamedFunctions": true,
                "allowUnboundThis": false
            }
        ],
        "prefer-const": [
            "error",
            {
                "destructuring": "all",
                "ignoreReadBeforeAssign": true
            }
        ],
        "prefer-reflect": [
            "error",
            {
                "exceptions": []
            }
        ],
        "prefer-rest-params": [
            "error"
        ],
        "prefer-spread": [
            "error"
        ],
        "prefer-template": [
            "error"
        ],
        "require-yield": [
            "error"
        ],
        "rest-spread-spacing": [
            "error",
            "never"
        ],
        "sort-imports": [
            "off"
        ],
        "template-curly-spacing": [
            "error",
            "never"
        ],
        "yield-star-spacing": [
            "error",
            {
                "before": false,
                "after": true
            }
        ]
    }
}

